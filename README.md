# 5-min-prod-app-python-web

This demo tests the [5 minute production app](https://about.gitlab.com/direction/5-min-production/) with a Python Flask web app in a container.

Useful resources:

- [5 Minute production app deploy template](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template)
- [AWS Deployment with GitLab CI/CD](https://docs.gitlab.com/ee/ci/cloud_deployment/)
- [Auto Deploy to AWS EC2 in GitLab 13.6](https://about.gitlab.com/releases/2020/11/22/gitlab-13-6-released/#auto-deploy-to-ec2)

Note: The AWS credentials need to be updated in the project settings in `Settings > CI/CD > Variables`: `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION`.


## Development

```
docker build . -t 5-min-prod-app-python-web

docker run -ti -p 5000:5000 5-min-prod-app-python-web
```

Open `http://0.0.0.0:5000/` and `http://0.0.0.0:5000/everyone-can-contribute` in your browser.
