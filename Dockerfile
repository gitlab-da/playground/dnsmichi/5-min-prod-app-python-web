FROM python:3

ADD . /src
RUN pip install -r /src/requirements.txt

WORKDIR /src
ENV PYTHONPATH '/src/'

EXPOSE 5000

CMD FLASK_APP=/src/app.py flask run --host=0.0.0.0 --port=5000

