import os
from flask import Flask 

app = Flask(__name__)

@app.route("/")
def main():
    return "Hello from GitLab :)"

@app.route("/everyone-can-contribute")
def hello():
    return "🦊"

if __name__ == "__main__":
    app.run()    
